<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Taller
 *
 * @ORM\Table(name="taller", uniqueConstraints={@ORM\UniqueConstraint(name="taller_pk", columns={"id_taller"})}, indexes={@ORM\Index(name="IDX_139F4584522BF898", columns={"id_programa"})})
 * @ORM\Entity
 */
class Taller
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_taller", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="taller_id_taller_seq", allocationSize=1, initialValue=1)
     */
    private $idTaller;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_taller", type="string", length=50, nullable=true)
     */
    private $nombreTaller;

    /**
     * @var \Programa
     *
     * @ORM\ManyToOne(targetEntity="Programa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_programa", referencedColumnName="id_programa")
     * })
     */
    private $idPrograma;



    /**
     * Get idTaller
     *
     * @return integer
     */
    public function getIdTaller()
    {
        return $this->idTaller;
    }

    /**
     * Set nombreTaller
     *
     * @param string $nombreTaller
     *
     * @return Taller
     */
    public function setNombreTaller($nombreTaller)
    {
        $this->nombreTaller = $nombreTaller;

        return $this;
    }

    /**
     * Get nombreTaller
     *
     * @return string
     */
    public function getNombreTaller()
    {
        return $this->nombreTaller;
    }

    /**
     * Set idPrograma
     *
     * @param \AppBundle\Entity\Programa $idPrograma
     *
     * @return Taller
     */
    public function setIdPrograma(\AppBundle\Entity\Programa $idPrograma = null)
    {
        $this->idPrograma = $idPrograma;

        return $this;
    }

    /**
     * Get idPrograma
     *
     * @return \AppBundle\Entity\Programa
     */
    public function getIdPrograma()
    {
        return $this->idPrograma;
    }
}
