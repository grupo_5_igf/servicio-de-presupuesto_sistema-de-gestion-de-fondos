<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Itempresupuesto
 *
 * @ORM\Table(name="itempresupuesto", uniqueConstraints={@ORM\UniqueConstraint(name="itempresupuesto_pk", columns={"id_item"})}, indexes={@ORM\Index(name="relationship_20_fk", columns={"id_presupuesto"}), @ORM\Index(name="relationship_21_fk", columns={"id_gasto"})})
 * @ORM\Entity
 */
class Itempresupuesto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_item", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="itempresupuesto_id_item_seq", allocationSize=1, initialValue=1)
     */
    private $idItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=true)
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="subtotal", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $subtotal;

    /**
     * @var \Presupuesto
     *
     * @ORM\ManyToOne(targetEntity="Presupuesto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_presupuesto", referencedColumnName="id_presupuesto")
     * })
     */
    private $idPresupuesto;

    /**
     * @var \Gastos
     *
     * @ORM\ManyToOne(targetEntity="Gastos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_gasto", referencedColumnName="id_gasto")
     * })
     */
    private $idGasto;



    /**
     * Get idItem
     *
     * @return integer
     */
    public function getIdItem()
    {
        return $this->idItem;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Itempresupuesto
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set subtotal
     *
     * @param string $subtotal
     *
     * @return Itempresupuesto
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return string
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set idPresupuesto
     *
     * @param \AppBundle\Entity\Presupuesto $idPresupuesto
     *
     * @return Itempresupuesto
     */
    public function setIdPresupuesto(\AppBundle\Entity\Presupuesto $idPresupuesto = null)
    {
        $this->idPresupuesto = $idPresupuesto;

        return $this;
    }

    /**
     * Get idPresupuesto
     *
     * @return \AppBundle\Entity\Presupuesto
     */
    public function getIdPresupuesto()
    {
        return $this->idPresupuesto;
    }

    /**
     * Set idGasto
     *
     * @param \AppBundle\Entity\Gastos $idGasto
     *
     * @return Itempresupuesto
     */
    public function setIdGasto(\AppBundle\Entity\Gastos $idGasto = null)
    {
        $this->idGasto = $idGasto;

        return $this;
    }

    /**
     * Get idGasto
     *
     * @return \AppBundle\Entity\Gastos
     */
    public function getIdGasto()
    {
        return $this->idGasto;
    }
}
