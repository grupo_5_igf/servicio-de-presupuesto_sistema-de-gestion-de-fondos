<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Presupuesto
 *
 * @ORM\Table(name="presupuesto", uniqueConstraints={@ORM\UniqueConstraint(name="presupuesto_pk", columns={"id_presupuesto"})}, indexes={@ORM\Index(name="relationship_13_fk", columns={"id_programa"})})
 * @ORM\Entity
 */
class Presupuesto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_presupuesto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="presupuesto_id_presupuesto_seq", allocationSize=1, initialValue=1)
     */
    private $idPresupuesto;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $total;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado_presupuesto", type="boolean", nullable=false)
     */
    private $estadoPresupuesto;

    /**
     * @var \Programa
     *
     * @ORM\ManyToOne(targetEntity="Programa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_programa", referencedColumnName="id_programa")
     * })
     */
    private $idPrograma;



    /**
     * Get idPresupuesto
     *
     * @return integer
     */
    public function getIdPresupuesto()
    {
        return $this->idPresupuesto;
    }

    public function setidPresupuesto($idPresupuesto){
    $this->idPresupuesto = $idPresupuesto;
    return $this;
  }
    /**
     * Set total
     *
     * @param string $total
     *
     * @return Presupuesto
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Presupuesto
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set estadoPresupuesto
     *
     * @param boolean $estadoPresupuesto
     *
     * @return Presupuesto
     */
    public function setEstadoPresupuesto($estadoPresupuesto)
    {
        $this->estadoPresupuesto = $estadoPresupuesto;

        return $this;
    }

    /**
     * Get estadoPresupuesto
     *
     * @return boolean
     */
    public function getEstadoPresupuesto()
    {
        return $this->estadoPresupuesto;
    }

    /**
     * Set idPrograma
     *
     * @param \AppBundle\Entity\Programa $idPrograma
     *
     * @return Presupuesto
     */
    public function setIdPrograma(\AppBundle\Entity\Programa $idPrograma = null)
    {
        $this->idPrograma = $idPrograma;

        return $this;
    }

    /**
     * Get idPrograma
     *
     * @return \AppBundle\Entity\Programa
     */
    public function getIdPrograma()
    {
        return $this->idPrograma;
    }

    /**
    * @var \Doctrine\Common\Collections\Collection
    * @ORM\OneToMany(targetEntity="Itempresupuesto", mappedBy="idPresupuesto")
    */
    private $itempresupuestos;

    /**
    * Get Itempresupuesto
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getItempresupuestos(){
      return $this->itempresupuestos;
    }
}
