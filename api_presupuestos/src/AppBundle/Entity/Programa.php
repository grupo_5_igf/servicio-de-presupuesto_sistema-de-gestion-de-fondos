<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Programa
 *
 * @ORM\Table(name="programa", uniqueConstraints={@ORM\UniqueConstraint(name="programa_pk", columns={"id_programa"})}, indexes={@ORM\Index(name="relationship_14_fk", columns={"id_presupuesto"})})
 * @ORM\Entity
 */
class Programa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_programa", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="programa_id_programa_seq", allocationSize=1, initialValue=1)
     */
    private $idPrograma;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_programa", type="string", length=50, nullable=true)
     */
    private $nombrePrograma;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var integer
     *
     * @ORM\Column(name="duracion", type="integer", nullable=true)
     */
    private $duracion;

    /**
     * @var \Presupuesto
     *
     * @ORM\ManyToOne(targetEntity="Presupuesto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_presupuesto", referencedColumnName="id_presupuesto")
     * })
     */
    private $idPresupuesto;



    /**
     * Get idPrograma
     *
     * @return integer
     */
    public function getIdPrograma()
    {
        return $this->idPrograma;
    }

    public function setidPrograma($idPrograma){
    $this->idPrograma = $idPrograma;
    return $this;
  }

    /**
     * Set nombrePrograma
     *
     * @param string $nombrePrograma
     *
     * @return Programa
     */
    public function setNombrePrograma($nombrePrograma)
    {
        $this->nombrePrograma = $nombrePrograma;

        return $this;
    }

    /**
     * Get nombrePrograma
     *
     * @return string
     */
    public function getNombrePrograma()
    {
        return $this->nombrePrograma;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     *
     * @return Programa
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set duracion
     *
     * @param integer $duracion
     *
     * @return Programa
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return integer
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set idPresupuesto
     *
     * @param \AppBundle\Entity\Presupuesto $idPresupuesto
     *
     * @return Programa
     */
    public function setIdPresupuesto(\AppBundle\Entity\Presupuesto $idPresupuesto = null)
    {
        $this->idPresupuesto = $idPresupuesto;

        return $this;
    }

    /**
     * Get idPresupuesto
     *
     * @return \AppBundle\Entity\Presupuesto
     */
    public function getIdPresupuesto()
    {
        return $this->idPresupuesto;
    }

    /**
    * @var \Doctrine\Common\Collections\Collection
    * @ORM\OneToMany(targetEntity="Taller", mappedBy="idPrograma")
    */
    private $talleres;

    /**
    * Get Talleres
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getTalleres(){
      return $this->talleres;
    }
}
