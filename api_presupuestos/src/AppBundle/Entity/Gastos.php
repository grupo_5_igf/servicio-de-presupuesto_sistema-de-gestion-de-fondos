<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gastos
 *
 * @ORM\Table(name="gastos", uniqueConstraints={@ORM\UniqueConstraint(name="gastos_pk", columns={"id_gasto"})})
 * @ORM\Entity
 */
class Gastos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_gasto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gastos_id_gasto_seq", allocationSize=1, initialValue=1)
     */
    private $idGasto;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_gasto", type="string", length=50, nullable=true)
     */
    private $nombreGasto;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $precio;



    /**
     * Get idGasto
     *
     * @return integer
     */
    public function getIdGasto()
    {
        return $this->idGasto;
    }

    public function setidGasto($idGasto){
    $this->idGasto = $idGasto;
    return $this;
  }

    /**
     * Set nombreGasto
     *
     * @param string $nombreGasto
     *
     * @return Gastos
     */
    public function setNombreGasto($nombreGasto)
    {
        $this->nombreGasto = $nombreGasto;

        return $this;
    }

    /**
     * Get nombreGasto
     *
     * @return string
     */
    public function getNombreGasto()
    {
        return $this->nombreGasto;
    }

    /**
     * Set precio
     *
     * @param string $precio
     *
     * @return Gastos
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }
}
