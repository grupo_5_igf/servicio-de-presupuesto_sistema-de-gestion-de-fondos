<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Taller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Taller controller.
 *
 * @Route("taller")
 */
class TallerController extends FOSRestController
{
    /**
     * Lists all taller entities.
     *
     * @Route("/", name="taller_index")
     * @Method("GET")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine();

      $talleres = $em->getRepository('AppBundle:Taller')->findAll();
      $statusCode=200;

      $view=$this->view($talleres,$statusCode);
        return $view;
    }

    /**
     * Creates a new taller entity.
     *
     * @Route("/new", name="taller_new")
     * @ParamConverter("taller", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Taller $taller)
    {
      $em = $this->getDoctrine()->getManager();
      $taller=$em->merge($taller);
      $em->persist($taller);
      $em->flush();

      $statusCode=200;
      $view=$this->view($taller,$statusCode);

        return  $this->handleView($view);
    }

    /**
     * Finds and displays a taller entity.
     *
     * @Route("/{idTaller}", name="taller_show")
     * @Method("GET")
     */
    public function showAction($idTaller)
    {
      $taller=$this->getDoctrine()->getRepository(Taller::class)->find($idTaller);
      if($taller != null){
        $statusCode=200;
        $view=$this->view($taller,$statusCode);
      return  $this->handleView($view);
    }else{
      throw new HttpException(400, "Taller no Encontrado.");
  }
    }

    /**
     * Displays a form to edit an existing taller entity.
     *
     * @Route("/{idTaller}/edit", name="taller_edit")
     * @ParamConverter("taller", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idTaller, Taller $taller)
    {
      $tallerR=$this->getDoctrine()->getRepository(Taller::class)->find($idTaller);

      if($tallerR != null){

        $tallerR->setNombreTaller($taller->getNombreTaller());

        $em=$this->getDoctrine()->getManager();
        $tallerR=$em->merge($tallerR);
        $em->persist($tallerR);
        $em->flush();

        $statusCode=200;
        $view=$this->view($tallerR,$statusCode);
      return  $this->handleView($view);
      }else{
          throw new HttpException(400, "Taller no Encontrado.");
      }
    }

    /**
     * Deletes a taller entity.
     *
     * @Route("/{idTaller}", name="taller_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Taller $taller)
    {
        $form = $this->createDeleteForm($taller);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($taller);
            $em->flush();
        }

        return $this->redirectToRoute('taller_index');
    }

    /**
     * Creates a form to delete a taller entity.
     *
     * @param Taller $taller The taller entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Taller $taller)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('taller_delete', array('idTaller' => $taller->getIdtaller())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
