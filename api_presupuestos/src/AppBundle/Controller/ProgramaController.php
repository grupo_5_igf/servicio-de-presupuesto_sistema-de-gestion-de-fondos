<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Programa;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Programa controller.
 *
 * @Route("programa")
 */
class ProgramaController extends FOSRestController
{
    /**
     * Lists all programa entities.
     *
     * @Route("/", name="programa_index")
     * @Method("GET")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine();

      $programas = $em->getRepository('AppBundle:Programa')->findAll();

      $statusCode=200;

      $view=$this->view($programas,$statusCode);
        return $view;
    }

    /**
     * Creates a new programa entity.
     *
     * @Route("/new", name="programa_new")
     * @ParamConverter("programa", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Programa $programa)
    {
      $em = $this->getDoctrine()->getManager();

      $em->persist($programa);
      $em->flush();

      $statusCode=200;
      $view=$this->view($programa,$statusCode);

        return  $this->handleView($view);
    }

    /**
     * Finds and displays a programa entity.
     *
     * @Route("/{idPrograma}", name="programa_show")
     * @Method("GET")
     */
    public function showAction($idPrograma)
    {
      $programa=$this->getDoctrine()->getRepository(Programa::class)->find($idPrograma);
      if($programa != null){
        $statusCode=200;
        $view=$this->view($programa,$statusCode);
      return  $this->handleView($view);
  }else{
    throw new HttpException(400, "Programa no Encontrado.");
  }
    }

    /**
     * Displays a form to edit an existing programa entity.
     *
     * @Route("/{idPrograma}/edit", name="programa_edit")
     * @ParamConverter("programa", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idPrograma, Programa $programa)
    {
      $programaR=$this->getDoctrine()->getRepository(Programa::class)->find($idPrograma);

      if($programaR != null){

        $programaR->setNombrePrograma($programa->getNombrePrograma());
        $programaR->setFechaInicio($programa->getFechaInicio());
        $programaR->setDuracion($programa->getDuracion());

        $em=$this->getDoctrine()->getManager();
        $programaR=$em->merge($programaR);
        $em->persist($programaR);
        $em->flush();

        $statusCode=200;
        $view=$this->view($programaR,$statusCode);
      return  $this->handleView($view);
      }else{
          throw new HttpException(400, "Programa no Encontrado.");
      }
    }

    /**
     * Deletes a programa entity.
     *
     * @Route("/{idPrograma}", name="programa_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Programa $programa)
    {
        $form = $this->createDeleteForm($programa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($programa);
            $em->flush();
        }

        return $this->redirectToRoute('programa_index');
    }

    /**
     * Creates a form to delete a programa entity.
     *
     * @param Programa $programa The programa entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Programa $programa)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('programa_delete', array('idPrograma' => $programa->getIdprograma())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
