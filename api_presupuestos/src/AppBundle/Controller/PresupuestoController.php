<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Presupuesto;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Presupuesto controller.
 *
 * @Route("presupuesto")
 */
class PresupuestoController extends FOSRestController
{
    /**
     * Lists all presupuesto entities.
     *
     * @Route("/", name="presupuesto_index")
     * @Method("GET")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine();

      $presupuestos = $em->getRepository('AppBundle:Presupuesto')->findAll();
      $statusCode=200;

      $view=$this->view($presupuestos,$statusCode);
        return $view;
      //return new JsonResponse($presupuestos);
    }

    /**
     * Creates a new presupuesto entity.
     *
     * @Route("/new", name="presupuesto_new")
     * @ParamConverter("presupuesto", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Presupuesto $presupuesto)
    {
      $em = $this->getDoctrine()->getManager();
      $presupuesto=$em->merge($presupuesto);
      $em->persist($presupuesto);
      $em->flush();

      $statusCode=200;
      $view=$this->view($presupuesto,$statusCode);

        return  $this->handleView($view);
    }

    /**
     * Finds and displays a presupuesto entity.
     *
     * @Route("/{idPresupuesto}", name="presupuesto_show")
     * @Method("GET")
     */
    public function showAction($idPresupuesto)
    {
      $presupuesto=$this->getDoctrine()->getRepository(Presupuesto::class)->find($idPresupuesto);
      if($presupuesto != null){
        $statusCode=200;
        $view=$this->view($presupuesto,$statusCode);
      return  $this->handleView($view);
  }else{
    throw new HttpException(400, "Presupuesto no Encontrado.");
  }
    }

    /**
     * Displays a form to edit an existing presupuesto entity.
     *
     * @Route("/{idPresupuesto}/edit", name="presupuesto_edit")
     * @ParamConverter("presupuesto", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idPresupuesto, Presupuesto $presupuesto)
    {
      $presupuestoR=$this->getDoctrine()->getRepository(Presupuesto::class)->find($idPresupuesto);

      if($presupuestoR != null){

        $presupuestoR->setTotal($presupuesto->getTotal());
        $presupuestoR->setFechaCreacion($presupuesto->getFechaCreacion());

        $em=$this->getDoctrine()->getManager();

        $em->persist($presupuestoR);
        $em->flush();

        $statusCode=200;
        $view=$this->view($presupuestoR,$statusCode);
      return  $this->handleView($view);
      }else{
          throw new HttpException(400, "Presupuesto no Encontrado.");
      }
    }

    /**
     * Deletes a presupuesto entity.
     *
     * @Route("/{idPresupuesto}", name="presupuesto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Presupuesto $presupuesto)
    {
        $form = $this->createDeleteForm($presupuesto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($presupuesto);
            $em->flush();
        }

        return $this->redirectToRoute('presupuesto_index');
    }

    /**
     * Creates a form to delete a presupuesto entity.
     *
     * @param Presupuesto $presupuesto The presupuesto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Presupuesto $presupuesto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('presupuesto_delete', array('idPresupuesto' => $presupuesto->getIdpresupuesto())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
