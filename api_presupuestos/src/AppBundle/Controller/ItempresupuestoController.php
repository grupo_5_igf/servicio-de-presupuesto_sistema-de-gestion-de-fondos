<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Itempresupuesto;
use AppBundle\Entity\Gastos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Itempresupuesto controller.
 *
 * @Route("itempresupuesto")
 */
class ItempresupuestoController extends FOSRestController
{
    /**
     * Lists all itempresupuesto entities.
     *
     * @Route("/", name="itempresupuesto_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $itempresupuestos = $em->getRepository('AppBundle:Itempresupuesto')->findAll();

        return $this->render('itempresupuesto/index.html.twig', array(
            'itempresupuestos' => $itempresupuestos,
        ));
    }

    /**
     * Creates a new itempresupuesto entity.
     *
     * @Route("/new", name="itempresupuesto_new")
     * @ParamConverter("itempresupuesto", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Itempresupuesto $itempresupuesto)
    {
      $em = $this->getDoctrine()->getManager();
      $itempresupuesto=$em->merge($itempresupuesto);
      $em->persist($itempresupuesto);
      $em->flush();

      $statusCode=200;
      $view=$this->view($itempresupuesto,$statusCode);

        return  $this->handleView($view);
    }

    /**
     * Finds and displays a itempresupuesto entity.
     *
     * @Route("/{idItem}", name="itempresupuesto_show")
     * @Method("GET")
     */
    public function showAction($idItem)
    {
      $itempresupuesto=$this->getDoctrine()->getRepository(Itempresupuesto::class)->find($idItem);
      if($itempresupuesto != null){
        $statusCode=200;
        $view=$this->view($itempresupuesto,$statusCode);
      return  $this->handleView($view);
  }else{
    throw new HttpException(400, "Item no Encontrado.");
  }
    }

    /**
     * Displays a form to edit an existing itempresupuesto entity.
     *
     * @Route("/{idItem}/edit", name="itempresupuesto_edit")
     * @ParamConverter("itempresupuesto", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idItem, Itempresupuesto $itempresupuesto)
    {
      $itempresupuestoR=$this->getDoctrine()->getRepository(Itempresupuesto::class)->find($idItem);

      if($itempresupuestoR != null){

        $itempresupuestoR->setCantidad($itempresupuesto->getCantidad());
        $itempresupuestoR->setSubtotal($itempresupuesto->getSubtotal());

        $em=$this->getDoctrine()->getManager();

        $em->persist($itempresupuestoR);
        $em->flush();

        $statusCode=200;
        $view=$this->view($itempresupuestoR,$statusCode);
      return  $this->handleView($view);
      }else{
          throw new HttpException(400, "Item no Encontrado.");
      }
    }

    /**
     * Deletes a itempresupuesto entity.
     *
     * @Route("/{idItem}", name="itempresupuesto_delete")
     * @Method("DELETE")
     */
    public function deleteAction($idItem)
    {
      $item=$this->getDoctrine()->getRepository(Itempresupuesto::class)->find($idItem);
          if($item != null){

              $em = $this->getDoctrine()->getManager();
              $em->remove($item);
              $em->flush();
          return new JsonResponse("Borrado con exito");
        }else{
        throw new HttpException(400, "Item no encontrado.");
        }
    }

    /**
     * Creates a form to delete a itempresupuesto entity.
     *
     * @param Itempresupuesto $itempresupuesto The itempresupuesto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Itempresupuesto $itempresupuesto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('itempresupuesto_delete', array('idItem' => $itempresupuesto->getIditem())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
