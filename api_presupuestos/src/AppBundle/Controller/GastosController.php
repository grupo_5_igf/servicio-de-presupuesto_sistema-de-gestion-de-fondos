<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Gastos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Gasto controller.
 *
 * @Route("gastos")
 */
class GastosController extends FOSRestController
{
    /**
     * Lists all gasto entities.
     *
     * @Route("/", name="gastos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine();

      $gastos = $em->getRepository('AppBundle:Gastos')->createQueryBuilder('q')->getQuery()->getArrayResult();

      return new JsonResponse($gastos);
    }

    /**
     * Creates a new gasto entity.
     *
     * @Route("/new", name="gastos_new")
     * @ParamConverter("gastos", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Gastos $gastos)
    {
      $em = $this->getDoctrine()->getManager();

      $em->persist($gastos);
      $em->flush();

      $statusCode=200;
      $view=$this->view($gastos,$statusCode);

        return  $this->handleView($view);
    }

    /**
     * Finds and displays a gasto entity.
     *
     * @Route("/{idGasto}", name="gastos_show")
     * @Method("GET")
     */
    public function showAction($idGasto)
    {
      $gasto=$this->getDoctrine()->getRepository(Gastos::class)->find($idGasto);
      if($gasto != null){
        $statusCode=200;
        $view=$this->view($gasto,$statusCode);
      return  $this->handleView($view);
  }else{
    throw new HttpException(400, "Gasto no Encontrado.");
  }
    }

    /**
     * Displays a form to edit an existing gasto entity.
     *
     * @Route("/{idGasto}/edit", name="gastos_edit")
     * @ParamConverter("gasto", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idGasto, Gastos $gasto)
    {
      $gastoR=$this->getDoctrine()->getRepository(Gastos::class)->find($idGasto);

      if($gastoR != null){

        $gastoR->setNombreGasto($gasto->getNombreGasto());
        $gastoR->setPrecio($gasto->getPrecio());

        $em=$this->getDoctrine()->getManager();

        $gastoR=$em->merge($gastoR);
        $em->persist($gastoR);
        $em->flush();

        $statusCode=200;
        $view=$this->view($gastoR,$statusCode);
      return  $this->handleView($view);
      }else{
          throw new HttpException(400, "Gasto no Encontrado.");
      }
    }

    /**
     * Deletes a gasto entity.
     *
     * @Route("/{idGasto}", name="gastos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Gastos $gasto)
    {
        $form = $this->createDeleteForm($gasto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gasto);
            $em->flush();
        }

        return $this->redirectToRoute('gastos_index');
    }

    /**
     * Creates a form to delete a gasto entity.
     *
     * @param Gastos $gasto The gasto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Gastos $gasto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gastos_delete', array('idGasto' => $gasto->getIdgasto())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
