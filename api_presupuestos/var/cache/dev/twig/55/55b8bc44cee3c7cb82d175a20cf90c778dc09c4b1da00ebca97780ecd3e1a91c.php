<?php

/* presupuesto/show.html.twig */
class __TwigTemplate_3d145c93ff9b68a0d5f8055486161ba7d1e7ff2a8587076336a3d879e26e67fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "presupuesto/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cab6e1796f4ccc13ed1d515289d58adeaef8a0eb7dcd4d3c934e581e18eef1fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cab6e1796f4ccc13ed1d515289d58adeaef8a0eb7dcd4d3c934e581e18eef1fb->enter($__internal_cab6e1796f4ccc13ed1d515289d58adeaef8a0eb7dcd4d3c934e581e18eef1fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "presupuesto/show.html.twig"));

        $__internal_9408537141f3c1abe5529c4091794bc9814d4078ce958feb5d8b7fd43ce74716 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9408537141f3c1abe5529c4091794bc9814d4078ce958feb5d8b7fd43ce74716->enter($__internal_9408537141f3c1abe5529c4091794bc9814d4078ce958feb5d8b7fd43ce74716_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "presupuesto/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cab6e1796f4ccc13ed1d515289d58adeaef8a0eb7dcd4d3c934e581e18eef1fb->leave($__internal_cab6e1796f4ccc13ed1d515289d58adeaef8a0eb7dcd4d3c934e581e18eef1fb_prof);

        
        $__internal_9408537141f3c1abe5529c4091794bc9814d4078ce958feb5d8b7fd43ce74716->leave($__internal_9408537141f3c1abe5529c4091794bc9814d4078ce958feb5d8b7fd43ce74716_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_eb65ccc7acbb84d083ab8b4a7d328cbb8e5d13ad580771cba740216b8827b350 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb65ccc7acbb84d083ab8b4a7d328cbb8e5d13ad580771cba740216b8827b350->enter($__internal_eb65ccc7acbb84d083ab8b4a7d328cbb8e5d13ad580771cba740216b8827b350_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bc64c28f60d7a2ec29bdbc5da367c50962f1df507a15b8e2231a96f0af805931 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc64c28f60d7a2ec29bdbc5da367c50962f1df507a15b8e2231a96f0af805931->enter($__internal_bc64c28f60d7a2ec29bdbc5da367c50962f1df507a15b8e2231a96f0af805931_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Presupuesto</h1>

    <table>
        <tbody>
            <tr>
                <th>Idpresupuesto</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["presupuesto"] ?? $this->getContext($context, "presupuesto")), "idPresupuesto", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Total</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["presupuesto"] ?? $this->getContext($context, "presupuesto")), "total", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Fechacreacion</th>
                <td>";
        // line 18
        if ($this->getAttribute(($context["presupuesto"] ?? $this->getContext($context, "presupuesto")), "fechaCreacion", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["presupuesto"] ?? $this->getContext($context, "presupuesto")), "fechaCreacion", array()), "Y-m-d"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Estadopresupuesto</th>
                <td>";
        // line 22
        if ($this->getAttribute(($context["presupuesto"] ?? $this->getContext($context, "presupuesto")), "estadoPresupuesto", array())) {
            echo "Yes";
        } else {
            echo "No";
        }
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("presupuesto_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("presupuesto_edit", array("idPresupuesto" => $this->getAttribute(($context["presupuesto"] ?? $this->getContext($context, "presupuesto")), "idPresupuesto", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_bc64c28f60d7a2ec29bdbc5da367c50962f1df507a15b8e2231a96f0af805931->leave($__internal_bc64c28f60d7a2ec29bdbc5da367c50962f1df507a15b8e2231a96f0af805931_prof);

        
        $__internal_eb65ccc7acbb84d083ab8b4a7d328cbb8e5d13ad580771cba740216b8827b350->leave($__internal_eb65ccc7acbb84d083ab8b4a7d328cbb8e5d13ad580771cba740216b8827b350_prof);

    }

    public function getTemplateName()
    {
        return "presupuesto/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 37,  106 => 35,  100 => 32,  94 => 29,  80 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Presupuesto</h1>

    <table>
        <tbody>
            <tr>
                <th>Idpresupuesto</th>
                <td>{{ presupuesto.idPresupuesto }}</td>
            </tr>
            <tr>
                <th>Total</th>
                <td>{{ presupuesto.total }}</td>
            </tr>
            <tr>
                <th>Fechacreacion</th>
                <td>{% if presupuesto.fechaCreacion %}{{ presupuesto.fechaCreacion|date('Y-m-d') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Estadopresupuesto</th>
                <td>{% if presupuesto.estadoPresupuesto %}Yes{% else %}No{% endif %}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('presupuesto_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('presupuesto_edit', { 'idPresupuesto': presupuesto.idPresupuesto }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "presupuesto/show.html.twig", "C:\\Users\\usuario\\Desktop\\api_presupuestos\\app\\Resources\\views\\presupuesto\\show.html.twig");
    }
}
