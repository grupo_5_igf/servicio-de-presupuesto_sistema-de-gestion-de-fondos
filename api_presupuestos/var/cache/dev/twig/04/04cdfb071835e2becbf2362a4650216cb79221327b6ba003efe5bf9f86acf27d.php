<?php

/* base.html.twig */
class __TwigTemplate_8eb128e1266ac9dd380c08c9f4bc5b6030f0f32aea9d6872e4144cc3e647cdfe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7fbf806a0a7e9e67bf356243774144c972874327f63bd718d6a07fc40648ca6d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7fbf806a0a7e9e67bf356243774144c972874327f63bd718d6a07fc40648ca6d->enter($__internal_7fbf806a0a7e9e67bf356243774144c972874327f63bd718d6a07fc40648ca6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_029cb810fc4e0f2ad527c1c0da9faac09487479266f2d4c05aebcdd7cd4e1d68 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_029cb810fc4e0f2ad527c1c0da9faac09487479266f2d4c05aebcdd7cd4e1d68->enter($__internal_029cb810fc4e0f2ad527c1c0da9faac09487479266f2d4c05aebcdd7cd4e1d68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_7fbf806a0a7e9e67bf356243774144c972874327f63bd718d6a07fc40648ca6d->leave($__internal_7fbf806a0a7e9e67bf356243774144c972874327f63bd718d6a07fc40648ca6d_prof);

        
        $__internal_029cb810fc4e0f2ad527c1c0da9faac09487479266f2d4c05aebcdd7cd4e1d68->leave($__internal_029cb810fc4e0f2ad527c1c0da9faac09487479266f2d4c05aebcdd7cd4e1d68_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_6fcf344e0489c55248b158105e5be1e61e595420c57e33f21ca0597ce03471ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fcf344e0489c55248b158105e5be1e61e595420c57e33f21ca0597ce03471ec->enter($__internal_6fcf344e0489c55248b158105e5be1e61e595420c57e33f21ca0597ce03471ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6d6bbfedada2d388dbd27970298ce461290d2fd9ee54b00d6c19e702473e6f6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d6bbfedada2d388dbd27970298ce461290d2fd9ee54b00d6c19e702473e6f6f->enter($__internal_6d6bbfedada2d388dbd27970298ce461290d2fd9ee54b00d6c19e702473e6f6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_6d6bbfedada2d388dbd27970298ce461290d2fd9ee54b00d6c19e702473e6f6f->leave($__internal_6d6bbfedada2d388dbd27970298ce461290d2fd9ee54b00d6c19e702473e6f6f_prof);

        
        $__internal_6fcf344e0489c55248b158105e5be1e61e595420c57e33f21ca0597ce03471ec->leave($__internal_6fcf344e0489c55248b158105e5be1e61e595420c57e33f21ca0597ce03471ec_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3d87da3bb1fe57bb7d4c67c7863ccf4b9fe2709327b2587b29356a0eb41b756a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d87da3bb1fe57bb7d4c67c7863ccf4b9fe2709327b2587b29356a0eb41b756a->enter($__internal_3d87da3bb1fe57bb7d4c67c7863ccf4b9fe2709327b2587b29356a0eb41b756a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_5902d9ae548019104a12725d507bcc72f112e57cf33b3cc358de230af944df03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5902d9ae548019104a12725d507bcc72f112e57cf33b3cc358de230af944df03->enter($__internal_5902d9ae548019104a12725d507bcc72f112e57cf33b3cc358de230af944df03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_5902d9ae548019104a12725d507bcc72f112e57cf33b3cc358de230af944df03->leave($__internal_5902d9ae548019104a12725d507bcc72f112e57cf33b3cc358de230af944df03_prof);

        
        $__internal_3d87da3bb1fe57bb7d4c67c7863ccf4b9fe2709327b2587b29356a0eb41b756a->leave($__internal_3d87da3bb1fe57bb7d4c67c7863ccf4b9fe2709327b2587b29356a0eb41b756a_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_8e1076d2adbb372cc6a096186e94f7676c7922da977b01ae9967d15824ea18d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e1076d2adbb372cc6a096186e94f7676c7922da977b01ae9967d15824ea18d4->enter($__internal_8e1076d2adbb372cc6a096186e94f7676c7922da977b01ae9967d15824ea18d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9ac7db99514aa7cb53301a52ff2554a61239230162cb6b0922f7a1522644a3ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ac7db99514aa7cb53301a52ff2554a61239230162cb6b0922f7a1522644a3ab->enter($__internal_9ac7db99514aa7cb53301a52ff2554a61239230162cb6b0922f7a1522644a3ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_9ac7db99514aa7cb53301a52ff2554a61239230162cb6b0922f7a1522644a3ab->leave($__internal_9ac7db99514aa7cb53301a52ff2554a61239230162cb6b0922f7a1522644a3ab_prof);

        
        $__internal_8e1076d2adbb372cc6a096186e94f7676c7922da977b01ae9967d15824ea18d4->leave($__internal_8e1076d2adbb372cc6a096186e94f7676c7922da977b01ae9967d15824ea18d4_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b40458bb36e43f98e4d10db761ab9e3ed8ebd838c8857dbd02271ed681dcbdd1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b40458bb36e43f98e4d10db761ab9e3ed8ebd838c8857dbd02271ed681dcbdd1->enter($__internal_b40458bb36e43f98e4d10db761ab9e3ed8ebd838c8857dbd02271ed681dcbdd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_67e0aa1abf9a7f5e1cdeadcfbc5e2c181ef2b8b7bd020c55247f14eaecef0434 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_67e0aa1abf9a7f5e1cdeadcfbc5e2c181ef2b8b7bd020c55247f14eaecef0434->enter($__internal_67e0aa1abf9a7f5e1cdeadcfbc5e2c181ef2b8b7bd020c55247f14eaecef0434_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_67e0aa1abf9a7f5e1cdeadcfbc5e2c181ef2b8b7bd020c55247f14eaecef0434->leave($__internal_67e0aa1abf9a7f5e1cdeadcfbc5e2c181ef2b8b7bd020c55247f14eaecef0434_prof);

        
        $__internal_b40458bb36e43f98e4d10db761ab9e3ed8ebd838c8857dbd02271ed681dcbdd1->leave($__internal_b40458bb36e43f98e4d10db761ab9e3ed8ebd838c8857dbd02271ed681dcbdd1_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "C:\\Users\\usuario\\Desktop\\api_presupuestos\\app\\Resources\\views\\base.html.twig");
    }
}
