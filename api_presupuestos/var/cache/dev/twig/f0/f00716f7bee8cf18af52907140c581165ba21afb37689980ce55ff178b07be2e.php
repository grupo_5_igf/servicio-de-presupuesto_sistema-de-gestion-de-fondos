<?php

/* gastos/show.html.twig */
class __TwigTemplate_77d919a63987f7a41eefe78bbd95bdaea193c49ebea7b10c5f8976abf92e4368 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "gastos/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_47313e43ba11dd4b84d5e71c319bb0fc482faa1b23c4353d4ff5c0866199c34f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47313e43ba11dd4b84d5e71c319bb0fc482faa1b23c4353d4ff5c0866199c34f->enter($__internal_47313e43ba11dd4b84d5e71c319bb0fc482faa1b23c4353d4ff5c0866199c34f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "gastos/show.html.twig"));

        $__internal_582c193e96572e5b5b5d1d71f2950b00e56adfdfe25c7a657c7b4aa3e487997c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_582c193e96572e5b5b5d1d71f2950b00e56adfdfe25c7a657c7b4aa3e487997c->enter($__internal_582c193e96572e5b5b5d1d71f2950b00e56adfdfe25c7a657c7b4aa3e487997c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "gastos/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_47313e43ba11dd4b84d5e71c319bb0fc482faa1b23c4353d4ff5c0866199c34f->leave($__internal_47313e43ba11dd4b84d5e71c319bb0fc482faa1b23c4353d4ff5c0866199c34f_prof);

        
        $__internal_582c193e96572e5b5b5d1d71f2950b00e56adfdfe25c7a657c7b4aa3e487997c->leave($__internal_582c193e96572e5b5b5d1d71f2950b00e56adfdfe25c7a657c7b4aa3e487997c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_a36623aecec0c00cce589f968aa5c3068818e87ccb5d563e72a237f9006d92a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a36623aecec0c00cce589f968aa5c3068818e87ccb5d563e72a237f9006d92a6->enter($__internal_a36623aecec0c00cce589f968aa5c3068818e87ccb5d563e72a237f9006d92a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_59cef97b95cef94f88a32c1c74a0a3fd1723f998d5dab513cb3ad6446f5541bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59cef97b95cef94f88a32c1c74a0a3fd1723f998d5dab513cb3ad6446f5541bf->enter($__internal_59cef97b95cef94f88a32c1c74a0a3fd1723f998d5dab513cb3ad6446f5541bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Gasto</h1>

    <table>
        <tbody>
            <tr>
                <th>Idgasto</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["gasto"] ?? $this->getContext($context, "gasto")), "idGasto", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombregasto</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["gasto"] ?? $this->getContext($context, "gasto")), "nombreGasto", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Precio</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["gasto"] ?? $this->getContext($context, "gasto")), "precio", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("gastos_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("gastos_edit", array("idGasto" => $this->getAttribute(($context["gasto"] ?? $this->getContext($context, "gasto")), "idGasto", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 33
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_59cef97b95cef94f88a32c1c74a0a3fd1723f998d5dab513cb3ad6446f5541bf->leave($__internal_59cef97b95cef94f88a32c1c74a0a3fd1723f998d5dab513cb3ad6446f5541bf_prof);

        
        $__internal_a36623aecec0c00cce589f968aa5c3068818e87ccb5d563e72a237f9006d92a6->leave($__internal_a36623aecec0c00cce589f968aa5c3068818e87ccb5d563e72a237f9006d92a6_prof);

    }

    public function getTemplateName()
    {
        return "gastos/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 33,  93 => 31,  87 => 28,  81 => 25,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Gasto</h1>

    <table>
        <tbody>
            <tr>
                <th>Idgasto</th>
                <td>{{ gasto.idGasto }}</td>
            </tr>
            <tr>
                <th>Nombregasto</th>
                <td>{{ gasto.nombreGasto }}</td>
            </tr>
            <tr>
                <th>Precio</th>
                <td>{{ gasto.precio }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('gastos_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('gastos_edit', { 'idGasto': gasto.idGasto }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "gastos/show.html.twig", "C:\\Users\\usuario\\Desktop\\api_presupuestos\\app\\Resources\\views\\gastos\\show.html.twig");
    }
}
