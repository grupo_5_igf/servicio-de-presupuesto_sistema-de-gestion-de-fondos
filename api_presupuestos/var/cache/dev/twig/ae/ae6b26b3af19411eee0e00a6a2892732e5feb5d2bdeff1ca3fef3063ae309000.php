<?php

/* programa/show.html.twig */
class __TwigTemplate_dfbab8c77749e2b563f2b39f0bafaa3e776a27c8e1a9edba9f0d5ac4d6b3f2bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "programa/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab88487d0218fd46a3bf9740bf4913fa1d7dc4e9ec04cf03d956bdb18294f9af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab88487d0218fd46a3bf9740bf4913fa1d7dc4e9ec04cf03d956bdb18294f9af->enter($__internal_ab88487d0218fd46a3bf9740bf4913fa1d7dc4e9ec04cf03d956bdb18294f9af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "programa/show.html.twig"));

        $__internal_487f5dba23c97a0083f86b9897fdababb0f0368b6c6d17f74fecde4bfc52f63f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_487f5dba23c97a0083f86b9897fdababb0f0368b6c6d17f74fecde4bfc52f63f->enter($__internal_487f5dba23c97a0083f86b9897fdababb0f0368b6c6d17f74fecde4bfc52f63f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "programa/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ab88487d0218fd46a3bf9740bf4913fa1d7dc4e9ec04cf03d956bdb18294f9af->leave($__internal_ab88487d0218fd46a3bf9740bf4913fa1d7dc4e9ec04cf03d956bdb18294f9af_prof);

        
        $__internal_487f5dba23c97a0083f86b9897fdababb0f0368b6c6d17f74fecde4bfc52f63f->leave($__internal_487f5dba23c97a0083f86b9897fdababb0f0368b6c6d17f74fecde4bfc52f63f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1d5a627f07fc41bc986e9e582afe62ae2b83068233d32394b10928480db08f4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d5a627f07fc41bc986e9e582afe62ae2b83068233d32394b10928480db08f4d->enter($__internal_1d5a627f07fc41bc986e9e582afe62ae2b83068233d32394b10928480db08f4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d84ad2b358e34066e425b1a6eaf74a62d039c96b9bff41420846941ad1256ff1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d84ad2b358e34066e425b1a6eaf74a62d039c96b9bff41420846941ad1256ff1->enter($__internal_d84ad2b358e34066e425b1a6eaf74a62d039c96b9bff41420846941ad1256ff1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Programa</h1>

    <table>
        <tbody>
            <tr>
                <th>Idprograma</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["programa"] ?? $this->getContext($context, "programa")), "idPrograma", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombreprograma</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["programa"] ?? $this->getContext($context, "programa")), "nombrePrograma", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Fechainicio</th>
                <td>";
        // line 18
        if ($this->getAttribute(($context["programa"] ?? $this->getContext($context, "programa")), "fechaInicio", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["programa"] ?? $this->getContext($context, "programa")), "fechaInicio", array()), "Y-m-d"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Duracion</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["programa"] ?? $this->getContext($context, "programa")), "duracion", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("programa_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("programa_edit", array("idPrograma" => $this->getAttribute(($context["programa"] ?? $this->getContext($context, "programa")), "idPrograma", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_d84ad2b358e34066e425b1a6eaf74a62d039c96b9bff41420846941ad1256ff1->leave($__internal_d84ad2b358e34066e425b1a6eaf74a62d039c96b9bff41420846941ad1256ff1_prof);

        
        $__internal_1d5a627f07fc41bc986e9e582afe62ae2b83068233d32394b10928480db08f4d->leave($__internal_1d5a627f07fc41bc986e9e582afe62ae2b83068233d32394b10928480db08f4d_prof);

    }

    public function getTemplateName()
    {
        return "programa/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 37,  102 => 35,  96 => 32,  90 => 29,  80 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Programa</h1>

    <table>
        <tbody>
            <tr>
                <th>Idprograma</th>
                <td>{{ programa.idPrograma }}</td>
            </tr>
            <tr>
                <th>Nombreprograma</th>
                <td>{{ programa.nombrePrograma }}</td>
            </tr>
            <tr>
                <th>Fechainicio</th>
                <td>{% if programa.fechaInicio %}{{ programa.fechaInicio|date('Y-m-d') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Duracion</th>
                <td>{{ programa.duracion }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('programa_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('programa_edit', { 'idPrograma': programa.idPrograma }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "programa/show.html.twig", "C:\\Users\\usuario\\Desktop\\api_presupuestos\\app\\Resources\\views\\programa\\show.html.twig");
    }
}
