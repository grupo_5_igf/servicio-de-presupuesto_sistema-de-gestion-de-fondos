<?php

/* @Twig/Exception/exception.json.twig */
class __TwigTemplate_38db984c2cfecf8d58c1676f3f0c707b4772204966a63a03797aa518e3fdfa8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d0f9c12aae8a92e42a2475190055090fda486bed9b27f475e7c5f378e6f0723 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d0f9c12aae8a92e42a2475190055090fda486bed9b27f475e7c5f378e6f0723->enter($__internal_3d0f9c12aae8a92e42a2475190055090fda486bed9b27f475e7c5f378e6f0723_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        $__internal_9c0edcbd403f63b38a325ed45f0d87557070ceb489f3d95aa3821ec69c78f007 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c0edcbd403f63b38a325ed45f0d87557070ceb489f3d95aa3821ec69c78f007->enter($__internal_9c0edcbd403f63b38a325ed45f0d87557070ceb489f3d95aa3821ec69c78f007_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_3d0f9c12aae8a92e42a2475190055090fda486bed9b27f475e7c5f378e6f0723->leave($__internal_3d0f9c12aae8a92e42a2475190055090fda486bed9b27f475e7c5f378e6f0723_prof);

        
        $__internal_9c0edcbd403f63b38a325ed45f0d87557070ceb489f3d95aa3821ec69c78f007->leave($__internal_9c0edcbd403f63b38a325ed45f0d87557070ceb489f3d95aa3821ec69c78f007_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "@Twig/Exception/exception.json.twig", "C:\\Users\\usuario\\Desktop\\api_presupuestos\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.json.twig");
    }
}
