<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // homepage
        if ('' === $trimmedPathinfo) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/gastos')) {
            // gastos_index
            if ('/gastos' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_gastos_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'gastos_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\GastosController::indexAction',  '_route' => 'gastos_index',);
            }
            not_gastos_index:

            // gastos_new
            if ('/gastos/new' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_gastos_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\GastosController::newAction',  '_route' => 'gastos_new',);
            }
            not_gastos_new:

            // gastos_show
            if (preg_match('#^/gastos/(?P<idGasto>[^/]++)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_gastos_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'gastos_show')), array (  '_controller' => 'AppBundle\\Controller\\GastosController::showAction',));
            }
            not_gastos_show:

            // gastos_edit
            if (preg_match('#^/gastos/(?P<idGasto>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if ('PUT' !== $canonicalMethod) {
                    $allow[] = 'PUT';
                    goto not_gastos_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'gastos_edit')), array (  '_controller' => 'AppBundle\\Controller\\GastosController::editAction',));
            }
            not_gastos_edit:

            // gastos_delete
            if (preg_match('#^/gastos/(?P<idGasto>[^/]++)$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_gastos_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'gastos_delete')), array (  '_controller' => 'AppBundle\\Controller\\GastosController::deleteAction',));
            }
            not_gastos_delete:

        }

        elseif (0 === strpos($pathinfo, '/itempresupuesto')) {
            // itempresupuesto_index
            if ('/itempresupuesto' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_itempresupuesto_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'itempresupuesto_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\ItempresupuestoController::indexAction',  '_route' => 'itempresupuesto_index',);
            }
            not_itempresupuesto_index:

            // itempresupuesto_new
            if ('/itempresupuesto/new' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_itempresupuesto_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\ItempresupuestoController::newAction',  '_route' => 'itempresupuesto_new',);
            }
            not_itempresupuesto_new:

            // itempresupuesto_show
            if (preg_match('#^/itempresupuesto/(?P<idItem>[^/]++)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_itempresupuesto_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'itempresupuesto_show')), array (  '_controller' => 'AppBundle\\Controller\\ItempresupuestoController::showAction',));
            }
            not_itempresupuesto_show:

            // itempresupuesto_edit
            if (preg_match('#^/itempresupuesto/(?P<idItem>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if ('PUT' !== $canonicalMethod) {
                    $allow[] = 'PUT';
                    goto not_itempresupuesto_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'itempresupuesto_edit')), array (  '_controller' => 'AppBundle\\Controller\\ItempresupuestoController::editAction',));
            }
            not_itempresupuesto_edit:

            // itempresupuesto_delete
            if (preg_match('#^/itempresupuesto/(?P<idItem>[^/]++)$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_itempresupuesto_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'itempresupuesto_delete')), array (  '_controller' => 'AppBundle\\Controller\\ItempresupuestoController::deleteAction',));
            }
            not_itempresupuesto_delete:

        }

        elseif (0 === strpos($pathinfo, '/presupuesto')) {
            // presupuesto_index
            if ('/presupuesto' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_presupuesto_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'presupuesto_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\PresupuestoController::indexAction',  '_route' => 'presupuesto_index',);
            }
            not_presupuesto_index:

            // presupuesto_new
            if ('/presupuesto/new' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_presupuesto_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\PresupuestoController::newAction',  '_route' => 'presupuesto_new',);
            }
            not_presupuesto_new:

            // presupuesto_show
            if (preg_match('#^/presupuesto/(?P<idPresupuesto>[^/]++)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_presupuesto_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'presupuesto_show')), array (  '_controller' => 'AppBundle\\Controller\\PresupuestoController::showAction',));
            }
            not_presupuesto_show:

            // presupuesto_edit
            if (preg_match('#^/presupuesto/(?P<idPresupuesto>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if ('PUT' !== $canonicalMethod) {
                    $allow[] = 'PUT';
                    goto not_presupuesto_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'presupuesto_edit')), array (  '_controller' => 'AppBundle\\Controller\\PresupuestoController::editAction',));
            }
            not_presupuesto_edit:

            // presupuesto_delete
            if (preg_match('#^/presupuesto/(?P<idPresupuesto>[^/]++)$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_presupuesto_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'presupuesto_delete')), array (  '_controller' => 'AppBundle\\Controller\\PresupuestoController::deleteAction',));
            }
            not_presupuesto_delete:

        }

        elseif (0 === strpos($pathinfo, '/programa')) {
            // programa_index
            if ('/programa' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_programa_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'programa_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\ProgramaController::indexAction',  '_route' => 'programa_index',);
            }
            not_programa_index:

            // programa_new
            if ('/programa/new' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_programa_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\ProgramaController::newAction',  '_route' => 'programa_new',);
            }
            not_programa_new:

            // programa_show
            if (preg_match('#^/programa/(?P<idPrograma>[^/]++)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_programa_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'programa_show')), array (  '_controller' => 'AppBundle\\Controller\\ProgramaController::showAction',));
            }
            not_programa_show:

            // programa_edit
            if (preg_match('#^/programa/(?P<idPrograma>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if ('PUT' !== $canonicalMethod) {
                    $allow[] = 'PUT';
                    goto not_programa_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'programa_edit')), array (  '_controller' => 'AppBundle\\Controller\\ProgramaController::editAction',));
            }
            not_programa_edit:

            // programa_delete
            if (preg_match('#^/programa/(?P<idPrograma>[^/]++)$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_programa_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'programa_delete')), array (  '_controller' => 'AppBundle\\Controller\\ProgramaController::deleteAction',));
            }
            not_programa_delete:

        }

        elseif (0 === strpos($pathinfo, '/taller')) {
            // taller_index
            if ('/taller' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_taller_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'taller_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\TallerController::indexAction',  '_route' => 'taller_index',);
            }
            not_taller_index:

            // taller_new
            if ('/taller/new' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_taller_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\TallerController::newAction',  '_route' => 'taller_new',);
            }
            not_taller_new:

            // taller_show
            if (preg_match('#^/taller/(?P<idTaller>[^/]++)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_taller_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'taller_show')), array (  '_controller' => 'AppBundle\\Controller\\TallerController::showAction',));
            }
            not_taller_show:

            // taller_edit
            if (preg_match('#^/taller/(?P<idTaller>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if ('PUT' !== $canonicalMethod) {
                    $allow[] = 'PUT';
                    goto not_taller_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'taller_edit')), array (  '_controller' => 'AppBundle\\Controller\\TallerController::editAction',));
            }
            not_taller_edit:

            // taller_delete
            if (preg_match('#^/taller/(?P<idTaller>[^/]++)$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_taller_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'taller_delete')), array (  '_controller' => 'AppBundle\\Controller\\TallerController::deleteAction',));
            }
            not_taller_delete:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
